# Possible solution

```
g++ -c library.cpp
ar rcs liblibrary.a library.o
g++ main.cpp -L. -llibrary

./a.out
```

The library is statically linked. Then I propose to use the command ``` objdump -t a.out ``` to get the symbol table.

It prints the following:
```
a.out:     formato del fichero elf64-x86-64

SYMBOL TABLE:
0000000000000000 l    df *ABS*  0000000000000000              main.cpp
0000000000000000 l    df *ABS*  0000000000000000              library.cpp
0000000000004151 l     O .bss   0000000000000001              _ZStL8__ioinit
00000000000011ab l     F .text  0000000000000052              _Z41__static_initialization_and_destruction_0ii
00000000000011fd l     F .text  0000000000000015              _GLOBAL__sub_I__Z8sayHellov
0000000000000000 l    df *ABS*  0000000000000000              
0000000000002014 l       .eh_frame_hdr  0000000000000000              __GNU_EH_FRAME_HDR
0000000000003da0 l     O .dynamic       0000000000000000              _DYNAMIC
0000000000003fe8 l     O .got.plt       0000000000000000              _GLOBAL_OFFSET_TABLE_
0000000000004030 g       .data  0000000000000000              _edata
0000000000004020  w      .data  0000000000000000              data_start
0000000000002000 g     O .rodata        0000000000000004              _IO_stdin_used
0000000000001179 g     F .text  0000000000000032              _Z8sayHellov
...
```

As we can see the first line references the main.cpp while the second line references the library file ```library.cpp```

Finally, a `grep` command can be used to filter only the library files. For example: `objdump -t a.out | grep '.cpp'`
And we get:

```
0000000000000000 l    df *ABS*  0000000000000000              main.cpp
0000000000000000 l    df *ABS*  0000000000000000              library.cpp
```